(function($){
    $.fn.ControlElementScroll=function(stateScrollDown = true){
        $('*').each(function(index, value){
            var element = $(this);
            var attrArcDOWN = element.attr('*down') || '';
            var dataArcDOWN = (attrArcDOWN.length>0) ? attrArcDOWN.split(';') : [];
            var attrArcUP = element.attr('*up') || '';
            var dataArcUP = (attrArcUP.length>0) ? attrArcUP.split(';') : [];
            var attrArcXSDOWN = element.attr('*xs-down') || '';
            var dataArcXSDOWN = (attrArcXSDOWN.length>0) ? attrArcXSDOWN.split(';') : [];
            var attrArcXSUP = element.attr('*xs-up') || '';
            var dataArcXSUP = (attrArcXSUP.length>0) ? attrArcXSUP.split(';') : [];
            var attrArcSMDOWN = element.attr('*sm-down') || '';
            var dataArcSMDOWN = (attrArcSMDOWN.length>0) ? attrArcSMDOWN.split(';') : [];
            var attrArcSMUP = element.attr('*sm-up') || '';
            var dataArcSMUP = (attrArcSMUP.length>0) ? attrArcSMUP.split(';') : [];
            var attrArcMDDOWN = element.attr('*md-down') || '';
            var dataArcMDDOWN = (attrArcMDDOWN.length>0) ? attrArcMDDOWN.split(';') : [];
            var attrArcMDUP = element.attr('*md-up') || '';
            var dataArcMDUP = (attrArcMDUP.length>0) ? attrArcMDUP.split(';') : [];
            var attrArcLGDOWN = element.attr('*lg-down') || '';
            var dataArcLGDOWN = (attrArcLGDOWN.length>0) ? attrArcLGDOWN.split(';') : [];
            var attrArcLGUP = element.attr('*lg-up') || '';
            var dataArcLGUP = (attrArcLGUP.length>0) ? attrArcLGUP.split(';') : [];
            var attrArcXLDOWN = element.attr('*xl-down') || '';
            var dataArcXLDOWN = (attrArcXLDOWN.length>0) ? attrArcXLDOWN.split(';') : [];
            var attrArcXLUP = element.attr('*xl-up') || '';
            var dataArcXLUP = (attrArcXLUP.length>0) ? attrArcXLUP.split(';') : [];
            
            var alldata = dataArcDOWN.concat(dataArcUP);
            alldata = alldata.concat(dataArcXSDOWN);
            alldata = alldata.concat(dataArcXSUP);
            alldata = alldata.concat(dataArcSMDOWN);
            alldata = alldata.concat(dataArcSMUP);
            alldata = alldata.concat(dataArcMDDOWN);
            alldata = alldata.concat(dataArcMDUP);
            alldata = alldata.concat(dataArcLGDOWN);
            alldata = alldata.concat(dataArcLGUP);
            alldata = alldata.concat(dataArcXLDOWN);
            alldata = alldata.concat(dataArcXLUP);
            //console.log(alldata);
            
            
            var widthFenetre = $(window).width();
            if(widthFenetre>=1200 && (dataArcXLDOWN.length > 0 || dataArcXLUP.length > 0)){
                element.removeClass(alldata.join(' '));
                if(stateScrollDown == true && dataArcXLDOWN.length > 0){
                    element.addClass(dataArcXLDOWN.join(' '));
                } else if(stateScrollDown == false && dataArcXLUP.length > 0) {
                    element.addClass(dataArcXLUP.join(' '));
                } 
            } else if(widthFenetre>=900 && (dataArcLGDOWN.length > 0 || dataArcLGUP.length > 0)){
                element.removeClass(alldata.join(' '));
                if(stateScrollDown == true && dataArcLGDOWN.length > 0){
                    element.addClass(dataArcLGDOWN.join(' '));
                } else if(stateScrollDown == false && dataArcLGUP.length > 0) {
                    element.addClass(dataArcLGUP.join(' '));
                } 
            } else if(widthFenetre>=700 && (dataArcMDDOWN.length > 0 || dataArcMDUP.length > 0)){
                element.removeClass(alldata.join(' '));
                if(stateScrollDown == true && dataArcMDDOWN.length > 0){
                    element.addClass(dataArcMDDOWN.join(' '));
                } else if(stateScrollDown == false && dataArcMDUP.length > 0) {
                    element.addClass(dataArcMDUP.join(' '));
                } 
            } else if(widthFenetre>=500 && (dataArcSMDOWN.length > 0 || dataArcSMUP.length > 0)){
                element.removeClass(alldata.join(' '));
                if(stateScrollDown == true && dataArcSMDOWN.length > 0){
                    element.addClass(dataArcSMDOWN.join(' '));
                } else if(stateScrollDown == false && dataArcSMUP.length > 0) {
                    element.addClass(dataArcSMUP.join(' '));
                } 
            } else if(widthFenetre>=0 && (dataArcXSDOWN.length > 0 || dataArcXSUP.length > 0)){
                element.removeClass(alldata.join(' '));
                if(stateScrollDown == true && dataArcXSDOWN.length > 0){
                    element.addClass(dataArcXSDOWN.join(' '));
                } else if(stateScrollDown == false && dataArcXSUP.length > 0) {
                    element.addClass(dataArcXSUP.join(' '));
                } 
            } else if(dataArcDOWN.length > 0 || dataArcUP.length > 0){
                element.removeClass(alldata.join(' '));
                if(stateScrollDown == true && dataArcDOWN.length > 0){
                    element.addClass(dataArcDOWN.join(' '));
                } else if(stateScrollDown == false && dataArcUP.length > 0) {
                    element.addClass(dataArcUP.join(' '));
                } 
            }
        });
    }
    $.fn.ControlElementArcMediaQuery=function(){
        $('*').each(function(index, value){
            var element = $(this);
            var attrArcXS = element.attr('*xs') || '';
            var dataArcXS = (attrArcXS.length>0) ? attrArcXS.split(';') : [];
            var attrArcSM = element.attr('*sm') || '';
            var dataArcSM = (attrArcSM.length>0) ? attrArcSM.split(';') : [];
            var attrArcMD = element.attr('*md') || '';
            var dataArcMD = (attrArcMD.length>0) ? attrArcMD.split(';') : [];
            var attrArcLG = element.attr('*lg') || '';
            var dataArcLG = (attrArcLG.length>0) ? attrArcLG.split(';') : [];
            var attrArcXL = element.attr('*xl') || '';
            var dataArcXL = (attrArcXL.length>0) ? attrArcXL.split(';') : [];
            var attrArcscroll_up = element.attr('*scroll-up') || '';
            var dataArcscroll_up = (attrArcscroll_up.length>0) ? attrArcscroll_up.split(';') : [];
            var attrArcscroll_down = element.attr('*scroll-down') || '';
            var dataArcscroll_down = (attrArcscroll_down.length>0) ? attrArcscroll_down.split(';') : [];
            var attrArcHover = element.attr('*hover') || '';
            var dataArcHover = (attrArcHover.length>0) ? attrArcHover.split(';') : [];

            var alldata = dataArcXS.concat(dataArcSM);
            alldata = alldata.concat(dataArcMD);
            alldata = alldata.concat(dataArcLG);
            alldata = alldata.concat(dataArcXL);
            //console.log(alldata.length);
            var alldatascroll = dataArcscroll_up.concat(dataArcscroll_down);
            //
            var alldatahover = [];
            alldatahover = alldatahover.concat(dataArcHover);
            
            
            //console.log('dataArcXS.length:: ' + dataArcXS.length);
            //console.log('dataArcSM.length:: ' + dataArcSM.length);
            //console.log('dataArcMD.length:: ' + dataArcMD.length);
            //console.log('dataArcLG.length:: ' + dataArcLG.length);
            //console.log('dataArcXL.length:: ' + dataArcXL.length);

            var widthFenetre = $(window).width();
            //console.log('widthFenetre:: ' + widthFenetre);

            if(widthFenetre>=1200 && dataArcXL.length > 0){
                element.removeClass(alldata.join(' '));
                element.addClass(dataArcXL.join(' '));
            } else if(widthFenetre>=900 && dataArcLG.length > 0){
                element.removeClass(alldata.join(' '));
                element.addClass(dataArcLG.join(' '));
            } else if(widthFenetre>=700 && dataArcMD.length > 0){
                element.removeClass(alldata.join(' '));
                element.addClass(dataArcMD.join(' '));
            } else if(widthFenetre>=500 && dataArcSM.length > 0){
                element.removeClass(alldata.join(' '));
                element.addClass(dataArcSM.join(' '));
            } else if(widthFenetre>=0 && dataArcXS.length > 0){
                element.removeClass(alldata.join(' '));
                element.addClass(dataArcXS.join(' '));
            }

            var scrollWindow = $(window).scrollTop();
            //console.log(scrollWindow);
            if(scrollWindow>=10 && dataArcscroll_down.length > 0){
                element.removeClass(alldatascroll.join(' '));
                element.addClass(dataArcscroll_down.join(' '));
            } else if(scrollWindow<10 && dataArcscroll_up.length > 0){
                element.removeClass(alldatascroll.join(' '));
                element.addClass(dataArcscroll_up.join(' '));
            }
            
            element.bind({
                "mouseenter": function(){
                    element.removeClass(alldatahover.join(' '));
                    element.addClass(dataArcHover.join(' '));
                },
                "mouseleave": function(){
                    element.removeClass(alldatahover.join(' '));
                }
            })
            
            //console.log(posScrollTop);
            
            //console.log(attrClass);
        });
    }
    $.fn.ArcMediaQuery=function(){
        $('body').ControlElementArcMediaQuery();
        $(window).on('resize scroll', function(e){
            $('body').ControlElementArcMediaQuery();
        });

        var posScrollTop2 = 0;
        var stateScrollDown = null;
        $(window).on('scroll', function(e){
            var posScrollTop1 = $(window).scrollTop();
            if(posScrollTop1 > posScrollTop2){
                stateScrollDown = true;
            } else {
                stateScrollDown = false;
            }
            $('body').ControlElementScroll(stateScrollDown);
            posScrollTop2 = posScrollTop1;
        });
    }
    $.fn.ArcScroll=function(typeAction = 'scrollTop', position = {}, duration = 500){
        var cible = $(this);
        var valPosElmtCible = 0;
        var keysPosition = Object.keys(position);
        var condScrollLeft = (typeAction=='scrollLeft' || typeAction=='scroll');
        var condScrollTop = (typeAction=='scrollTop' || typeAction=='scroll');
        if(  
            keysPosition.length>0 &&
            (
                (keysPosition.includes('x') && condScrollLeft) ||
                (keysPosition.includes('y') && condScrollTop)
            )
        ){
            if(condScrollTop){
                valPosElmtCible = position.y;
                $('body,html').animate({
                    scrollTop : valPosElmtCible
                }, duration);
            } else if(condScrollLeft){
                valPosElmtCible = position.x;
                $('body,html').animate({
                    scrollLeft : valPosElmtCible
                }, duration);
            }
        } else {
            //console.log("cible:: " + JSON.stringify(cible));
            cible.each(function(index, value){
                var elmtCible = $(this);
                var topPosElmtCible = elmtCible.offset().top;
                var leftPosElmtCible = elmtCible.offset().left;
                ////console.log("-- ArcScroll --");
                //console.log("condScrollTop:: " + condScrollTop);
                //console.log("elmtCible:: " + JSON.stringify(elmtCible));
                //console.log("topPosElmtCible:: " + topPosElmtCible);
                //console.log("elmtCible.offset():: " + JSON.stringify(elmtCible.offset()));
                if(condScrollTop){
                    $('body,html').animate({
                        scrollTop : topPosElmtCible
                    }, duration);
                } else if(condScrollLeft){
                    $('body,html').animate({
                        scrollLeft : leftPosElmtCible
                    }, duration);
                }
            });
        }
    }
    $.fn.ArcToggle=function(typeAction = 'toggle', duration=400 , direction = 'top'){
        var classesInlineBlock = '*anim *anim-permanent';
        var cible = $(this);
        cible.each(function(index, value){
            var elmtCible = $(this);

            var numberShow = ($(this).attr('style') || '').indexOf('display: none') ==-1;
            var stateShow = numberShow;
            //console.log(numberShow);
            //console.log(stateShow);
            if(typeAction == 'toggle'){
                if(direction=='all'){
                    elmtCible.toggle(duration);
                } else if(direction=='left' || direction=='right'){
                    elmtCible.animate({width: 'toggle'});
                } else if(direction=='top' || direction=='bottom'){
                    elmtCible.slideToggle(duration);
                }
            } else if(stateShow){
                if(typeAction == 'hide'){
                    if(direction=='all'){
                        elmtCible.hide(duration);
                    } else if(direction=='left' || direction=='right'){
                        elmtCible.animate({width: 'hide'});
                    } else if(direction=='top' || direction=='bottom'){
                        elmtCible.slideUp();
                    }
                } else if(typeAction == 'fade' || typeAction == 'fadeOut'){
                    elmtCible.fadeOut(duration);
                }
            } else{
                if(typeAction == 'show'){
                    if(direction=='all'){
                        elmtCible.show();
                    } else if(direction=='left' || direction=='right'){
                        elmtCible.animate({width: 'top'});
                    } else if(direction=='top' || direction=='bottom'){
                        elmtCible.slideDown();
                    }
                } else if(typeAction == 'fade' || typeAction == 'fadeIn'){
                    elmtCible.fadeIn(duration);
                }
            }
        });
    }
    $.fn.ArcLoader=function(time = 5000){
        $(this).each(function(index, value){
            $(this).hide().ajaxStart(function(){
                $(this).fadeIn();
            }).ajaxStop(function(){
                $(this).fadeOut(time);
            });
        });
    }
    $.fn.ArcValidationForm=function(validationButton){
        var DisableSubmit = function(target, disableState){
            if(target.length>0){
                if(disableState) {
                    target.attr("disabled", "true");
                } else {
                    target.removeAttr("disabled");
                }
            }
        }
        var ValidationFunct = function(field, form, selectorValidationButton){
            var formElement = $(form);
            var validStateForm = form.checkValidity();
            var actionForm = formElement.attr('action');

            var fieldElement = $(field);
            var nameFieldElement = fieldElement.attr('name');
            var choicesValString = fieldElement.attr('field-choice') || '';
            var choicesVal = (choicesValString.length>0) ? choicesValString.split(',') : [];
            var SLCTR_messageForField = formElement.find(".message-validation[for='"+nameFieldElement+"']");
            var SLCTR_messageForForm = formElement.find(".message-validation[for='"+formElement+"']");
            var messageForForm = ((SLCTR_messageForForm.text()).length>0) ? (SLCTR_messageForForm.text() || formElement.validationMessage) : formElement.validationMessage;
            //console.log(SLCTR_messageForField);
            if(
                choicesVal.length>0
            ){
                //console.log("choicesVal:: " + choicesVal);
                if((choicesVal).includes(fieldElement.val())){
                    field.setCustomValidity("");
                } else {
                    var newMSG = 'valeur(s) possible(s): ' + (choicesVal).map(function(data){
                        return "'" + data + "'";
                    }).join(' or ');
                    field.setCustomValidity(newMSG);
                }
            }
            var messageForField = ((SLCTR_messageForField.text()).length>0) ? (SLCTR_messageForField.text() || field.validationMessage) : field.validationMessage;
            var validStateField = field.checkValidity(); 
            
            //console.log(SLCTR_messageForField);
            if(!validStateForm){
                if(SLCTR_messageForForm.length>0){
                    SLCTR_messageForForm.text(messageForForm);
                    SLCTR_messageForForm.show();
                }
                if(SLCTR_messageForField.length>0){
                    SLCTR_messageForField.text(messageForField);
                    SLCTR_messageForField.show();
                }
                formElement.find(".message-validation").show();
                DisableSubmit(selectorValidationButton, true);
            } else {
                if(SLCTR_messageForForm.length>0){
                    SLCTR_messageForForm.hide();
                }
                if(SLCTR_messageForField.length>0){
                    SLCTR_messageForField.hide();
                }
                fieldElement.removeClass('*invalid-field');
                formElement.find(".message-validation").hide();
                DisableSubmit(selectorValidationButton, false);
            }
        };
        $(this).each(function(index, value){
            var form = this;
            var formElement = $(form);
            //console.log(this);
            var selectorValidationButton = formElement.find("input[type='submit'][name='"+validationButton+"'], button[type='submit'][name='"+validationButton+"']");
            var allDivFields = formElement.find(".field");
            var allFields = formElement.find("input:not(input[type='submit']), select, textarea");
            var arcMessageValidation = formElement.find(".message-validation");
            console.log(arcMessageValidation.length);
            arcMessageValidation.fadeOut();
            allFields.each(function(index, value){
                var fieldElement = $(this);
                fieldElement.on('click', function() {
                    allDivFields.addClass('field-validation');
                    arcMessageValidation.show();
                });
                ValidationFunct(this, form, selectorValidationButton);
                fieldElement.on('change paste keyup click', function(){
                    ValidationFunct(this, form, selectorValidationButton);
                });
            });
        });
    }
})(jQuery);
$(document).ready(function(){
    $('.me-badge-toggle').button('toggle');
    $('form[name="form-edit"]').ArcValidationForm('validator-btn');
    $("body").ArcMediaQuery();
    $(".alert").alert();
    $('.carousel').carousel({
        interval: 25000
    });
    $("form[name='form-login-simple']").ArcValidationForm("button-validation");
    $('.dateTimepicker').flatpickr({
        enableTime: true,
        dateFormat: "Y-m-d H:i"
    });
    $('.timepicker').flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        //minTime: "16:00",
        //maxTime: "22:30",
        //defaultDate: "13:45"
    });
    $('.datepicker').flatpickr({
        altInput: true,
        altFormat: "d-m-Y",
        dateFormat: "Y-m-d",
        weekNumbers: true,
        //inline: true,
        //minDate: "today",
        //maxDate:"15.12.2017",
        //disable: [
        //    "2025-01-30",
        //    "2025-02-21",
        //    "2025-03-08",
        //    new Date(2025, 4, 9),
        //    {
        //        from: "2025-09-01",
        //        to: "2025-12-01"
        //    },
        //],
        //enable: ["2025-03-30", "2025-05-21", "2025-06-08", new Date(2025, 8, 9) ]
    });
    $('.datepicker-multiple').flatpickr({
        mode: "multiple",
        dateFormat: "Y-m-d",
        conjunction: " :: ",
        //defaultDate: ["2016-10-20", "2016-11-04"]
    });
    $('.datepicker-range').flatpickr({
        mode: "range"
    });
    $('#edenin-loader').ArcLoader();
    $('.navdrawer').navdrawer('hide');
});