import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HandleError } from '../others/handle-error';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService<T> {
  protected apiUrl = 'api/kindles';
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'Application/json'})
  };

  constructor(private http: HttpClient) { }
  
  GetElement(): Observable<T[]> {
    return this.http.get<T[]>(this.apiUrl)
    .pipe(
      retry(1),
      catchError(HandleError)
    );
  }
  PostElement(element: T): Observable<T> {
    return this.http.post<T>(this.apiUrl, element, this.httpOptions)
    .pipe(
      retry(1),
      catchError(HandleError)
    );
  }
  DeleteElement(idElement: string): Observable<T> {
    return this.http.delete<T>(this.apiUrl + '/' + idElement, this.httpOptions)
    .pipe(
      retry(1),
      catchError(HandleError)
    );
  }
}
