import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OtherService {

  constructor() { }
  
  RemoveDuplicates (array: any[]) {
    let uniqueArray: any[] = [];
    for ( let i = 0 ; i < array.length ; i++ ) {
      if ( uniqueArray.indexOf(array[i]) === -1 ) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }
  DateDiff(dateOld: Date, dateNew: Date, type: 'year' | 'month' | 'week' | 'day' | 'hour'): number {
    let dateDiff: number = dateNew.getTime() - dateOld.getTime();
    if(type=='year') {
      return dateDiff/86400000;
    } else if(type=='month') {
      return dateDiff/(2.62*Math.pow(10, 9));
    }  else if(type=='week') {
      return dateDiff/(6.04*Math.pow(10, 8));
    } else if(type=='day') {
      return dateDiff/(8.64*Math.pow(10, 7));
    }  else if(type=='hour') {
      return dateDiff/(3.6*Math.pow(10, 6));
    } else {
      return 0;
    }
  }
  SortAsc(a: any, b: any) {
    if(typeof(a)=='number' && typeof(b)=='number') {
      return a-b;
    } else {
      return (a as string).toString().localeCompare((b as string).toString());
    }
  }
  SortDesc(a: any, b: any) {
    if (typeof(a)=='number' && typeof(b)=='number') {
      return b-a;
    } else {
      return (b as string).toString().localeCompare((a as string).toString());
    }
  }
}
