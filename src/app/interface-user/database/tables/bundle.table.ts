import { BundleInterface } from '../../interfaces/bundle.interface';

export const BundleTable: BundleInterface[] = [
  {
    id: 'qsdhj45',
    slug: 'aziue55',
    name: 'bundle 1',
    url: 'bundle-default.jpg',
    dateAdded: '2020-02-13'
  },
  {
    id: 'stryt45',
    slug: 'chvhds41546',
    name: 'bundle 2',
    url: 'bundle-default.jpg',
    dateAdded: '2019-11-20'
  },
  {
    id: 'otpoy784',
    slug: 'opieqsh754',
    name: 'bundle 3',
    url: 'bundle-default.jpg',
    dateAdded: '2018-11-20'
  }
];