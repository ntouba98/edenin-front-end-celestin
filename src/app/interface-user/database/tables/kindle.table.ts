import { KindleInterface } from '../../interfaces/kindle.interface';

export let KindleTable: KindleInterface[] = [
    {
        id: 'qsh78',
        slug: 'sdsqd4566',
        title: 'The Dare',
        author: ['Lauren Landish'],
        logo: 'the-dare-pp.jpg',
        price: 3.99,
        description: 'Have you ever had one of those really bad days at work? You know, one where your hot boss catches you photocopying your backside in his office? No? Just me then? ',
        dateAdded: '2020-02-14',
        dateCreated: '2015-06-04',
        category: 'enfant',
        tag: ['educatif', 'aventure'],
        content: [
            {
                language: 'goumala',
                page: [
                    {
                        text: 'le premier texte de the dare en francais',
                        image: 'the-dare-img1.png'
                    },
                    {
                        text: 'texte de fin'
                    },
                    {
                        image: 'the-dare-img2.png'
                    }
                ]
            },
            {
                language: 'en',
                page: [
                    {
                        text: 'the first text of the dare en francais',
                        image: 'the-dare-img1.png'
                    },
                    {
                        text: 'end\'s text'
                    },
                    {
                        image: 'the-dare-img2.png'
                    }
                ]
            }
        ]
    },
    {
        id: 'jds45',
        slug: 'aze15156',
        title: 'The Seeker: Irin Chronicles Book Seven',
        author: ['Elizabeth Hunter'],
        logo: 'the-seeker-pp.jpg',
        audio: 'the-seeker-audio.mp3',
        price: 0.99,
        description: 'C\'est un scribe qui cherche des réponses. Si seulement un chanteur méfiant - et frustrant de séduction - le laissait aider. Invoqué sur la côte du golfe de Louisiane, Rhys of Glast, archiviste Irin et scribe d\'Istanbul, doit convaincre une légendaire chanteuse Irina de lui faire confiance. Son succès pourrait changer l\'équilibre',
        dateAdded: '2020-01-24',
        dateCreated: '2015-06-04',
        category: 'enfant',
        tag: ['educatif', 'action'],
        content: [
            {
                language: 'fr',
                page: [
                    {
                        text: 'le premier texte de the dare en francais',
                        image: 'the-seeker-img1.png'
                    },
                    {
                        text: 'texte de fin'
                    },
                    {
                        image: 'the-seeker-img2.png'
                    }
                ]
            },
            {
                language: 'en',
                page: [
                    {
                        text: 'the first text of the dare en francais',
                        image: 'the-seeker-img1.png'
                    },
                    {
                        text: 'end\'s text'
                    },
                    {
                        image: 'the-seeker-img2.png'
                    }
                ]
            }
        ]
    },
    {
        id: 'erti7',
        slug: 'lslk5121',
        title: 'The Forest of Forever',
        author: ['Rob Blackwell'],
        logo: 'the-forest-forever-pp.jpg',
        audio: 'the-forest-forever-audio.mp3',
        price: 'free',
        description: 'C\'est un scribe qui cherche des réponses. Si seulement un chanteur méfiant - et frustrant de séduction - le laissait aider. Invoqué sur la côte du golfe de Louisiane, Rhys of Glast, archiviste Irin et scribe d\'Istanbul, doit convaincre une légendaire chanteuse Irina de lui faire confiance. Son succès pourrait changer l\'équilibre',
        dateAdded: '2019-05-02',
        dateCreated: '2015-06-04',
        category: 'enfant',
        tag: ['horror', 'drame'],
        content: [
            {
                language: 'fr',
                page: [
                    {
                        text: 'le premier texte de the dare en francais',
                        image: 'the-forest-forever-img1.png'
                    },
                    {
                        text: 'texte de fin'
                    },
                    {
                        image: 'the-forest-forever-img2.png'
                    }
                ]
            },
            {
                language: 'bassa',
                page: [
                    {
                        text: 'the first text of the dare en francais',
                        image: 'the-forest-forever-img1.png'
                    },
                    {
                        text: 'end\'s text'
                    },
                    {
                        image: 'the-forest-forever-img2.png'
                    }
                ]
            }
        ]
    }
];

KindleTable = KindleTable.map((data) => {
    data.freeState = (data.price=='free') ? true : false;
    return data;
});