import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { KindleInterface } from '../interfaces/kindle.interface';
import { BundleInterface } from '../interfaces/bundle.interface';
import { BundleTable } from './tables/bundle.table';
import { KindleTable } from './tables/kindle.table';



@Injectable({
  providedIn: 'root'
})
export class DbServiceService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const kindles: KindleInterface[] = KindleTable;
    const printables: BundleInterface[] = BundleTable;

    return {kindles, printables};
  }
}
