import { InMemoryDbService } from 'angular-in-memory-web-api';
import { KindleInterface } from '../interfaces/kindle.interface';
import { BundleInterface } from '../interfaces/bundle.interface';
import { BundleTable } from './tables/bundle.table';
import { KindleTable } from './tables/kindle.table';

export class Database implements InMemoryDbService {

    createDb() {
      const stories: KindleInterface[] = KindleTable;
      const printables: BundleInterface[] = BundleTable;
  
      return {stories, printables};
    }
}