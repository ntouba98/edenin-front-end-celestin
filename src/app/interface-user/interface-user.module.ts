import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { StoriesComponent } from './components/stories/stories.component';
import { PrintablesComponent } from './components/printables/printables.component';
import { DetailStoryComponent } from './components/detail-story/detail-story.component';
const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', redirectTo: '/', pathMatch: 'full' },
  { path: 'stories', component: StoriesComponent },
  { path: 'printables', component: PrintablesComponent },
  { path: 'detail-story/:id', component: DetailStoryComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class InterfaceUserModule { }
