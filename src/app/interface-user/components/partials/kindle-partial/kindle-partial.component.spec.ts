import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KindlePartialComponent } from './kindle-partial.component';

describe('KindlePartialComponent', () => {
  let component: KindlePartialComponent;
  let fixture: ComponentFixture<KindlePartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KindlePartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KindlePartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
