import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from 'src/app/interface-user/services/http.service';
import { KindleInterface } from 'src/app/interface-user/interfaces/kindle.interface';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import StringSup from '@ntouba98/quicktools/other/String';
import { OtherService } from 'src/app/interface-user/services/other.service';

const stringSup: StringSup = new StringSup();

@Component({
  selector: 'app-kindle-partial',
  templateUrl: './kindle-partial.component.html',
  styleUrls: ['./kindle-partial.component.sass']
})
export class KindlePartialComponent extends HttpService<KindleInterface> implements OnInit {
  protected apiUrl = 'api/stories';
  protected element: KindleInterface;
  protected elements: KindleInterface[];
  protected elementLength: number;
  protected elementExist: Boolean = true;

  //header
  @Input()
  protected title: string = 'stories';
  @Input()
  protected logo: string = '';

  //structure
  @Input()
  protected titleState: boolean = true;
  @Input()
  protected paginationState: boolean = true;
  @Input()
  protected sortFilterState: boolean = true;

  //url params
  @Input()
  protected urlRedirect: string = '/stories';
  protected queriesParams: {
    page: string,
    sort: string,
    search: string,
    tag: string,
    language: string,
    sizeContentMin: string,
    sizeContentMax: string,
    date: string
  } = {
    page: 'page',
    sort: 'sort',
    search: 'search',
    tag: 'tag',
    language: 'language',
    sizeContentMin: 'sizeContentMin',
    sizeContentMax: 'sizeContentMax',
    date: 'date'
  };

  //pagination
  @Input()
  protected nbreByPage: number = 30;
  protected currentPage: number = 1;
  private lengthPage: number = 1;

  //sort
  @Input()
  protected typeSort: 'descDate' | 'ascDate' | 'descPrice' | 'ascPrice' | null = null;

  //filter
  //filter>search
  protected search: string | null = null;
  private searchFinal: string[] = [];
  //filter>tag
  protected tags: {name: string, checked: boolean}[] = [];
  protected tagChoice: string[] = [];
  //filter>language
  protected languages: {name: string, checked: boolean}[] = [];
  protected languageChoice: string[] = [];
  //filter>sizeContent
  protected sizeContents: {name: string, min?: number, max?: number, checked: boolean}[] = [
    {name: '0-100', min: 0, max: 100, checked: false},
    {name: '101-1000', min: 101, max: 1000, checked: false},
    {name: '1001-5000', min: 1001, max: 5000, checked: false},
    {name: 'plus de 5000', min: 5001, checked: false}
  ];
  protected sizeContentChoiceMin: number | null = null;
  protected sizeContentChoiceMax: number | null = null;
  //filter>date
  protected dates: {name: string, value: number | null}[] = [
    {name: 'any date', value: null},
    {name: 'today', value: 1},
    {name: 'this week', value: 7},
    {name: 'this month', value: 30},
    {name: 'this year', value: 365}
  ];
  protected dateChoice: number | null = null;

  constructor(
    http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private otherService: OtherService
  ) {
    super(http);
  }

  async ngOnInit() {
    this.GetQueryParams();
    this.TagFilter();
    this.LanguageFilter();
    this.LoadData();
    this.elements = this.elements;
  }

  //other
  NavigateModifyUrlData(url= this.urlRedirect) {
    let queryParams = {};
    if ( this.currentPage>1 ) {
      queryParams[this.queriesParams.page] = this.currentPage;
    }
    if ( this.typeSort && this.typeSort.length>0 ) {
      queryParams[this.queriesParams.sort] = this.typeSort;
    }
    if ( this.searchFinal && this.searchFinal.length>0 ) {
      queryParams[this.queriesParams.search] = this.searchFinal.join('+');
    }
    if ( this.tagChoice && this.tagChoice.length>0 ) {
      queryParams[this.queriesParams.tag] = this.tagChoice.join('+');
    }
    if ( this.languageChoice && this.languageChoice.length>0 ) {
      queryParams[this.queriesParams.language] = this.languageChoice.join('+');
    }
    if ( this.sizeContentChoiceMin ) {
      queryParams[this.queriesParams.sizeContentMin] = this.sizeContentChoiceMin;
    }
    if ( this.sizeContentChoiceMax ) {
      queryParams[this.queriesParams.sizeContentMax] = this.sizeContentChoiceMax;
    }
    if ( this.dateChoice ) {
      queryParams[this.queriesParams.date] = this.dateChoice;
    }

    this.router.navigate([url], {
      queryParams: queryParams
    });
  }
  GetQueryParams(){
    this.activatedRoute.queryParams.subscribe((queryParams)=>{
      this.currentPage = (parseInt(queryParams[this.queriesParams.page])) ? Math.ceil(parseInt(queryParams[this.queriesParams.page])) : 1;
      this.currentPage = (this.currentPage<1) ? 1 : this.currentPage;
      this.currentPage = (this.currentPage>this.lengthPage) ? this.lengthPage : this.currentPage;
      this.Sort(queryParams[this.queriesParams.sort]);
      //filter>search
      this.searchFinal = this.FilterActionStepQueryParams(queryParams[this.queriesParams.search], 'keyword');
      //filter>tag
      this.tagChoice = this.FilterActionStepQueryParams(queryParams[this.queriesParams.tag], 'keyword');
      //filter>language
      this.languageChoice = this.FilterActionStepQueryParams(queryParams[this.queriesParams.language], 'keyword');
      //filter>sizeContent
      this.sizeContentChoiceMin = this.FilterActionStepQueryParams(queryParams[this.queriesParams.sizeContentMin], 'number-interval');
      this.sizeContentChoiceMax = this.FilterActionStepQueryParams(queryParams[this.queriesParams.sizeContentMax], 'number-interval');
      //filter>date
      this.dateChoice = queryParams[this.queriesParams.date] || null;
    });
  }
  async LoadData() {
    this.GetElement().subscribe((data) => {
      //filter>search
      if (this.searchFinal.length > 0) {
        data = data.filter((element: KindleInterface) => {
          let result: boolean = false;
          this.searchFinal.forEach((search) => {
            if (element.title.toLowerCase().indexOf(search) != -1) {
              result = true;
            }
          });
          return result;
        });
      }
      //filter>tag
      if (this.tagChoice.length > 0) {
        data = data.filter((element: KindleInterface) => {
          let result: boolean = false;
          this.tagChoice.forEach((tag) => {
            if (element.tag.includes(tag)) {
              result = true;
            }
          });
          return result;
        });
      }
      //filter>sizeContent
      if (this.sizeContentChoiceMin || this.sizeContentChoiceMax) {
        //console.log(`sizeContentChoiceMin:: ${this.sizeContentChoiceMin}`);
        //console.log(`sizeContentChoiceMax:: ${this.sizeContentChoiceMax}`);
        data = data.filter((element: KindleInterface, index: number) => {
          let result: boolean = false;
          //console.log(`index:: ${index}`);
          //console.log(`element.id:: ${element.id}`);
          //console.log(`element.title:: ${element.title}`);
          //console.log(`element.content.length:: ${element.content.length}`);
          const cond1: boolean = (
            ( this.sizeContentChoiceMin && this.sizeContentChoiceMax ) &&
            this.sizeContentChoiceMin <= element.content.length &&
            this.sizeContentChoiceMax >= element.content.length
          );
          const cond2: boolean = (
            ( this.sizeContentChoiceMin && !this.sizeContentChoiceMax ) &&
            this.sizeContentChoiceMin <= element.content.length
          );
          const cond3: boolean = (
            ( !this.sizeContentChoiceMin && this.sizeContentChoiceMax ) &&
            this.sizeContentChoiceMax >= element.content.length
          );

          if (cond1) {
            result = true;
          } else if (cond2) {
            result = true;
          } else if (cond3) {
            result = true;
          }

          return result;
        });
      }
      //filter>language
      if (this.languageChoice.length > 0) {
        data = data.filter((element: KindleInterface) => {
          let result: boolean = false;
          const languageElement = element.content.map((val)=>{
            return val.language.toString();
          });
          this.languageChoice.forEach((language) => {
            if (languageElement.includes(language)) {
              result = true;
            }
          });
          return result;
        });
      }
      //filter>date
      if (this.dateChoice) {
        data = data.filter((element: KindleInterface) => {
          let result: boolean = false;
          let dateDiff: number = this.otherService.DateDiff(new Date(element.dateAdded), new Date(), 'day');
          if (dateDiff <= this.dateChoice) {
            result = true;
          }
          return result;
        });
      }

      data = data.sort((a: KindleInterface, b: KindleInterface) => {
        if (this.typeSort == 'descDate') {
          return a.dateAdded.localeCompare(b.dateAdded);
        } else if (this.typeSort == 'ascDate') {
          return b.dateAdded.localeCompare(a.dateAdded);
        } else if (this.typeSort == 'descPrice') {
          if( typeof(a.price) == 'number' && typeof(b.price) == 'number' ) {
            return b.price - a.price;
          } else {
            return b.dateAdded.localeCompare(a.dateAdded);
          }
        } else if (this.typeSort == 'ascPrice') {
          if( typeof(a.price) == 'number' && typeof(b.price) == 'number' ) {
            return a.price - b.price;
          } else {
            return a.dateAdded.localeCompare(b.dateAdded);
          }
        } else {
          return a.id.localeCompare(b.id);
        }
      });

      this.elementLength = data.length;
      this.elementExist = (this.elementLength>0) ? true : false;
      this.lengthPage = (this.elementLength && this.elementLength>0) ? (this.elementLength / this.nbreByPage) : 1;
      this.lengthPage = Math.ceil(this.lengthPage);

      const max = this.nbreByPage*this.currentPage;
      const min = max-this.nbreByPage;
      data = data.filter((value: KindleInterface, index: number)=>{
        return (index>=min && index<max);
      });
      this.elements = data;
      //console.log(`this.tags: ${JSON.stringify(this.tags)}`);
      //console.log(`this.languages: ${JSON.stringify(this.languages)}`);
      //console.log(`this.sizeContents: ${JSON.stringify(this.sizeContents)}`);
      //console.log(`this.languages: ${JSON.stringify(this.languages)}`);
      //console.log(`this.lengthPage: ${this.lengthPage}`);
      //console.log(`this.nbreByPage: ${this.nbreByPage}`);
      //console.log(`this.currentPage: ${this.currentPage}`);
      //console.log(`min: ${min} et max: ${max}`);
      //console.log(data);
    });
  }

  //filter
  Filter() {
    //filter>search
    this.searchFinal = this.FilterActionStepForm(this.search);

    //filter>tags
    const tags: string[] = this.tags.filter((data)=>{
      return (data.checked);
    }).map( (data) => {
      return data.name.toString();
    } );
    this.tagChoice = this.FilterActionStepForm(tags, 'checkbox') as string[];

    //filter>sizeContent
    const sizeContentsMin: number[] = this.sizeContents.filter((data)=>{
      return (data.checked);
    }).map( (data) => {
      return data.min;
    } );
    const sizeContentsMax: number[] = this.sizeContents.filter((data)=>{
      return (data.checked);
    }).map( (data) => {
      return data.max;
    } );
    this.sizeContentChoiceMin = this.FilterActionStepForm(sizeContentsMin, 'number-interval-min');
    this.sizeContentChoiceMax = this.FilterActionStepForm(sizeContentsMax, 'number-interval-max');

    //filter>languages
    const languages: string[] = this.languages.filter((data)=>{
      return (data.checked);
    }).map( (data) => {
      return data.name.toString();
    } );
    this.languageChoice = this.FilterActionStepForm(languages, 'checkbox') as string[];

    //filter>dates
    this.dateChoice = this.dateChoice;

    this.LoadData();
    this.NavigateModifyUrlData();

    //console.log(`search:: ${JSON.stringify(this.searchFinal)}`);
    //console.log(`tags:: ${JSON.stringify(this.tags)}`);
    //console.log(`tagChoice:: ${JSON.stringify(this.tagChoice)}`);
    //console.log(`languages:: ${JSON.stringify(this.languages)}`);
    //console.log(`languageChoice:: ${JSON.stringify(this.languageChoice)}`);
  }
  FilterActionStepOnInit(value: string[], type: 'checkbox' = 'checkbox'): {name: string, checked: boolean}[] {
    if(type=='checkbox') {
      let value1: string[] = this.otherService.RemoveDuplicates(value);
      const result: {name: string, checked: boolean}[] = value1.map((element) => {
        element = element.ReplaceAll(' ', '').toString().toLowerCase();
        return {name: element, checked: false};
      });
      return result
    } else {
      return [];
    }
  }
  FilterActionStepQueryParams(
    value,
    type: 'keyword' | 'number-interval' | 'interval' = 'keyword'
  ): any {
    if (type=='keyword') {
      const elementsAuthorize: any[] = [' ', '\'', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 1, 2, 3, 4, 5, 6, 7, 8 ,9];
      
      value = ((value) ? value : '').toLowerCase();
  
      const valueStart: string[] = value.split('');
      valueStart.forEach((data) => {
        if ( !elementsAuthorize.includes( data ) ) {
          value = value.ReplaceAll(data, '');
        }
      });
      value = value.ReplaceAll('\'', ' ');
  
      value = value.ReplaceAll(' ', '+');

      const result: string[] = value.split('+').filter((data) => {
        return (data.length > 0);
      });
  
      return result;
    } else if (type == 'number-interval') {
      let result: number | null = null;
      if (value && parseInt(value) && value.length > 0) {
        result = value;
      }
      return result;
    } else if (type == 'interval') {
      let result: string | null = null;
      if (value && value.length > 0) {
        result = value as string;
      }
      return result;
    } else {
      return [];
    }
  }
  FilterActionStepForm(
    value,
    type: 'keyword' | 'checkbox' | 'radio' | 'number-interval-min' | 'number-interval-max' | 'interval-min' | 'interval-max' = 'keyword'
  ): any {
    if(type=='keyword') {
      return this.FilterActionStepQueryParams(value, type);
    } else if(type=='checkbox') {
      let result: any[] = value as any[];
      return result;
    } else if(type=='radio') {
      let result: string = null;
      if(value) {
        result = value;
      }
      return result;
    } else if (type == 'number-interval-min') {
      let result: number | null = null;
      if (value && value.length > 0) {
        result = ((value as any[]).sort(this.otherService.SortAsc))[0];
      }
      return result;
    } else if (type == 'number-interval-max') {
      let result: number | null = null;
      if (value && value.length > 0) {
        result = ((value as any[]).sort(this.otherService.SortDesc))[0];
      }
      return result;
    } else if (type == 'interval-min') {
      let result: string | null = null;
      if (value && value.length > 0) {
        result = ((value as string[]).sort(this.otherService.SortAsc))[0];
      }
      return result;
    } else if (type == 'interval-max') {
      let result: string | null = null;
      if (value && value.length > 0) {
        result = ((value as string[]).sort(this.otherService.SortDesc))[0];
      }
      return result;
    }
  }
  //filter>search
  //filter>tag
  TagFilter() {
    this.GetElement().subscribe((data) => {
      let tags: string[] = [];
      data.forEach( (element: KindleInterface) => {
        tags = tags.concat(element.tag);
      } );
      this.tags = this.FilterActionStepOnInit(tags, 'checkbox');
    });
  }
  //filter>language
  LanguageFilter() {
    this.GetElement().subscribe((data) => {
      let languages: string[] = [];
      data.forEach( (element: KindleInterface) => {
        languages = languages.concat(element.content.map((element)=>{
          return element.language;
        }));
      } );
      this.languages = this.FilterActionStepOnInit(languages, 'checkbox');
    });
  }
  //filter>date

  //sorting
  SortFunct(a: KindleInterface, b: KindleInterface) {
    if (this.typeSort && this.typeSort == 'descDate') {
      return a.dateAdded.localeCompare(b.dateAdded);
    } else if (this.typeSort && this.typeSort == 'ascDate') {
      return b.dateAdded.localeCompare(a.dateAdded);
    } else if (this.typeSort && this.typeSort == 'descPrice') {
      if( typeof(a.price) == 'number' && typeof(b.price) == 'number' ) {
        return b.price - a.price;
      } else {
        return a.dateAdded.localeCompare(b.dateAdded);
      }
    } else if (this.typeSort && this.typeSort == 'ascPrice') {
      if( typeof(a.price) == 'number' && typeof(b.price) == 'number' ) {
        return a.price - b.price;
      } else {
        return b.dateAdded.localeCompare(a.dateAdded);
      }
    } else {
      return a.id.localeCompare(b.id);
    }
  }
  Sort(val: 'descDate' | 'ascDate' | 'descPrice' | 'ascPrice' | null = null){
    this.typeSort = (val == 'descDate' || val == 'ascDate' || val == 'descPrice' || val == 'ascPrice' || val == null) ? val : null;
    this.LoadData();
    this.NavigateModifyUrlData();
  }

  //pagination
  PageFunct(){}
  ChangePage(val: number | 'suiv' | 'prec') {
    if(typeof(val)=='number'){
      this.currentPage = (parseInt(val.toString())) ? Math.ceil(parseInt(val.toString())) : 1;
    } else if(val=='suiv') {
      this.currentPage++;
    } else if(val=='prec') {
      this.currentPage--;
    } else {
      this.currentPage = 1;
    }
    this.currentPage = (this.currentPage<1) ? 1 : this.currentPage;
    this.currentPage = (this.currentPage>this.lengthPage) ? this.lengthPage : this.currentPage;

    this.LoadData();

    this.NavigateModifyUrlData();
  }
  Counter(i: number) {
    return new Array(i);
  }
  GeneratePages(): any[] {
    return this.Counter(this.lengthPage);
  }
  ActivePage(i: number=1): boolean{
    return (i == this.currentPage);
  }
  DisabledPreviousNextState(nextState: boolean = true): boolean {
    if(nextState){
      return (this.currentPage==1);
    } else {
      return (this.currentPage==this.lengthPage);
    }
  }
}