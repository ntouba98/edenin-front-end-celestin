import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BundlePartialComponent } from './bundle-partial.component';

describe('BundlePartialComponent', () => {
  let component: BundlePartialComponent;
  let fixture: ComponentFixture<BundlePartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BundlePartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BundlePartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
