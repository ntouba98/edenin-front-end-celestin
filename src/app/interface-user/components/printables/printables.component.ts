import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-printables',
  templateUrl: './printables.component.html',
  styleUrls: ['./printables.component.sass']
})
export class PrintablesComponent implements OnInit {
  protected urlRedirect: string = '/printables';
  protected title: string = 'printables';
  protected logo: string = 'local_movies';

  constructor() { }

  ngOnInit() {
  }

}
