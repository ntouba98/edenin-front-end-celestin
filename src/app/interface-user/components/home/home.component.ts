import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  //
  protected title: string = 'homepage';
  protected logo: string = 'home';
  //kindles
  protected kindleUrlRedirect: string = '/';
  protected kindleTitleState: boolean = false;
  protected kindlePaginationState: boolean = false;
  protected kindleSortFilterState: boolean = false;
  protected kindleNbreByPage: number = 6;
  protected kindleTypeSort: string = 'descDate';
  //bundles
  protected bundleUrlRedirect: string = '/';
  protected bundleTitleState: boolean = false;
  protected bundlePaginationState: boolean = false;
  protected bundleSortFilterState: boolean = false;
  protected bundleNbreByPage: number = 6;
  protected bundleTypeSort: string = 'descDate';

  constructor() { }

  ngOnInit() {
  }

}
