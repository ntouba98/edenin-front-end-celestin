import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.sass']
})
export class StoriesComponent {
  protected urlRedirect: string = '/stories';
  protected title: string = 'stories';
  protected logo: string = 'menu_book';

  constructor() {
  }

  async ngOnInit() {
  }
}
