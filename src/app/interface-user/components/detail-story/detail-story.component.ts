import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { KindleInterface } from '../../interfaces/kindle.interface';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { OtherService } from '../../services/other.service';

@Component({
  selector: 'app-detail-story',
  templateUrl: './detail-story.component.html',
  styleUrls: ['./detail-story.component.sass']
})
export class DetailStoryComponent extends HttpService<KindleInterface> implements OnInit {
  protected apiUrl = 'api/stories';
  protected element: KindleInterface;
  protected elements: KindleInterface[];
  protected elementLength: number;
  protected elementExist: Boolean = true;
  protected idElement: string = '';

  protected languages: string[] = [];
  protected authors: string = '';
  protected audioState: boolean = false;
  protected freeState: boolean = false;
  protected tags: any[] = [];
  protected price: number | 'free' = 0;
  protected title: string = '';
  protected description: string = '';
  protected logo: string = '';
  
  constructor(
    http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private otherService: OtherService
  ) {
    super(http);
  }

  async ngOnInit() {
    this.GetQueryParams();
    this.LoadData();
    this.elements = this.elements;
  }

  async LoadData() {
    this.GetElement().subscribe((data) => {
      data = data.sort((a: KindleInterface, b: KindleInterface) => {
        return a.id.localeCompare(b.id);
      });

      this.elements = data;

      this.element = this.elements.find((value: KindleInterface)=>{
        return (value.slug == this.idElement);
      });
      //freeState
      this.freeState = (this.element.freeState) ? true : false;
      //tags
      this.tags = this.element.tag;
      //price
      this.price = this.element.price;
      //title
      this.title = this.element.title;
      //description
      this.description = this.element.description;
      //logo
      this.logo = this.element.logo;
      //language
      this.languages = this.element.content.map( (value) => {
        return value.language;
      } );
      this.languages = this.otherService.RemoveDuplicates(this.languages) as string[];
      //author
      this.authors = this.element.author.join(', ');
      //audio
      this.audioState = (this.element.audio && this.element.audio.length>0) ? true : false;



      this.elementLength = data.length;
      this.elementExist = (this.elementLength>0) ? true : false;

      //console.log(`this.element:: ${JSON.stringify(this.element)}`);
    });
  }
  GetQueryParams(){
    this.idElement = this.activatedRoute.snapshot.paramMap.get('id');
  }

}
