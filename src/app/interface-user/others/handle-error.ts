import { throwError } from 'rxjs';

export function HandleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(JSON.stringify(errorMessage));
    window.alert(errorMessage);
    return throwError(errorMessage);
}