export interface BundleInterface {
    id: string;
    slug: string;
    name: string;
    url: string;
    dateAdded: string;
}