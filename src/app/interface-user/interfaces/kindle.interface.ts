export interface KindleInterface{
    id: string;
    slug: string;
    title: string;
    author: string[];
    logo: string;
    audio?: string;
    price: number | 'free';
    freeState?: boolean,
    category: string,
    description: string;
    dateAdded: string;
    dateCreated: string;
    tag: string[];
    content: KindleContentInterface[];
}
export interface KindleContentInterface{
    language: 'fr' | 'en' | 'bassa' | 'goumala';
    page: KindleContentPageInterface[];
}
export interface KindleContentPageInterface{
    text?: string | null;
    image?: string | null;
}