import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'edenin-front-end-by-celestin';
  private urlParent: string = '';

  constructor(private activatedRoute: ActivatedRoute, private location: Location) {}

  ngOnInit() {
    this.urlParent = this.location.path();
  }

  protected TestUrl(route: string = '', totalState: boolean | 'first' = true): boolean {
    let result: boolean = false;
    //console.log(`--> this.urlParent:: <${this.urlParent}>`);
    //console.log(`route:: <${route}>`);
    //console.log(`totalState:: <${totalState}>`);
    //console.log('----------------------------------');
    if(totalState == 'first' && route.length==1 && this.urlParent.indexOf(route)==0) {
      result = true;
    } else if (!totalState && route.length>1 && this.urlParent.indexOf(route)==0) {
      result = true;
    } else if (totalState && route.length>1 && this.urlParent == route) {
      result = true;
    }

    return result;
  }
}
