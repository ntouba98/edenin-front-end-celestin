import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientInMemoryWebApiModule, InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { Database } from './interface-user/database/database';
import { HttpErrorInterceptor } from './interface-user/others/http-error.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InterfaceUserModule } from './interface-user/interface-user.module';

import { HomeComponent } from './interface-user/components/home/home.component';
import { StoriesComponent } from './interface-user/components/stories/stories.component';
import { PrintablesComponent } from './interface-user/components/printables/printables.component';
import { DetailStoryComponent } from './interface-user/components/detail-story/detail-story.component';
import { KindlePartialComponent } from './interface-user/components/partials/kindle-partial/kindle-partial.component';
import { BundlePartialComponent } from './interface-user/components/partials/bundle-partial/bundle-partial.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StoriesComponent,
    PrintablesComponent,
    DetailStoryComponent,
    KindlePartialComponent,
    BundlePartialComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    InterfaceUserModule,
    FormsModule,
    HttpClientInMemoryWebApiModule.forRoot(Database, {dataEncapsulation: false})
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
